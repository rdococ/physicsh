# physicsh mod for Minetest

The physicsh mod for Minetest adds an API that allows mods to modify player physics without overriding the physics of other mods. Simply depend/opt-depend on physicsh, use the API, and you're good to go.

## Usage

The mod adds five functions which can be accessed in the global physicsh table.

`player` can either be the PlayerRef itself or the player name. `values` can either be nil to remove a modifier, or a table containing speed, jump and/or gravity values.

* `physicsh.set_multiplier(player, id, values)`: Sets the multiplication modifier `id` to `values`.
* `physicsh.set_adder(player, id, values)`: Sets the addition modifier `id` to `values`.
* `physicsh.get_multiplier(player, id)`: Gets the multiplication modifier `id`.
* `physicsh.get_adder(player, id)`: Gets the addition modifier `id`.
* `physicsh.recalculate(player)`: Recalculates the physics override for `player`.

Multipliers are applied before adders, so if you wear 2x speed boots and +1 speed thrusters then your speed will be 1\*2+1 = 3, rather than (1+1)*2 = 4. For this example at least this makes sense; boots that help you to walk faster should have no effect on thrusters that do not use your legs to propel you.

This API may change any time - and indeed, the information above may not be accurate to the latest version of physicsh - but if enough mods come to depend on it then I will ensure that I don't break backwards compatibility.

TenPlus1 also made a mod which you should check out: [pova on GitHub](https://github.com/tenplus1/pova). They do basically the same thing but go about it in different ways.
