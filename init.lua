local internal = {}
physicsh = {}

local function recalculate_override(name, do_full_recalculation)
	local datum = internal[name]
	
	local override
	
	local multipliers = datum.multipliers
	local adders = datum.adders
	
	if do_full_recalculation then
		local recalculated = {
			speed = 1,
			jump = 1,
			gravity = 1
		}
		for id, multiplier in pairs(multipliers) do
			recalculated.speed = recalculated.speed * (multiplier.speed or 1)
			recalculated.jump = recalculated.jump * (multiplier.jump or 1)
			recalculated.gravity = recalculated.gravity * (multiplier.gravity or 1)
		end
		datum.multiplication_total = recalculated
	end
	override = table.copy(datum.multiplication_total)
	
	if do_full_recalculation then
		local recalculated = {
			speed = 0,
			jump = 0,
			gravity = 0
		}
		for id, adder in pairs(adders) do
			recalculated.speed = recalculated.speed + (adder.speed or 0)
			recalculated.jump = recalculated.jump + (adder.jump or 0)
			recalculated.gravity = recalculated.gravity + (adder.gravity or 0)
		end
		datum.addition_total = recalculated
	end
	
	local addition_total = datum.addition_total
	override.speed = override.speed + addition_total.speed
	override.jump = override.jump + addition_total.jump
	override.gravity = override.gravity + addition_total.gravity
	
	return override
end

local function reset_internal(name)
	internal[name] = {
		multipliers = {},
		adders = {},
		multiplication_total = {
			speed = 1,
			jump = 1,
			gravity = 1
		},
		addition_total = {
			speed = 0,
			jump = 0,
			gravity = 0
		}
	}
end

minetest.register_on_joinplayer(function (player)
	reset_internal(player:get_player_name())
end)
minetest.register_on_respawnplayer(function (player)
	reset_internal(player:get_player_name())
	player:set_physics_override({
		speed = 1,
		jump = 1,
		gravity = 1
	})
end)
minetest.register_on_leaveplayer(function (player)
	internal[player:get_player_name()] = nil
end)

physicsh.set_multiplier = function (name, id, values)
	if not name then return end
	
	local player
	if type(name) ~= "string" then
		player = name
		name = player:get_player_name()
	else
		player = minetest.get_player_by_name(name)
		if not player then return end
	end
	
	local data = internal[name]
	
	if not data then return end
	if not id then return end
	
	local multipliers = data.multipliers
	local multiplication_total = data.multiplication_total
		
	local old_modifier, new_modifier = multipliers[id] or {}, values or {}
	multiplication_total = multiplication_total or {}
	
	multipliers[id] = new_modifier
	
	for property, old_value in pairs(multiplication_total) do
		data.multiplication_total[property] = old_value * (new_modifier[property] or 1) / (old_modifier[property] or 1)
	end
		
	player:set_physics_override(recalculate_override(name))
	return true
end
physicsh.set_adder = function (name, id, values)
	if not name then return end
	
	local player
	if type(name) ~= "string" then
		player = name
		name = player:get_player_name()
	else
		player = minetest.get_player_by_name(name)
		if not player then return end
	end
	
	local data = internal[name]
	
	if not data then return end
	if not id then return end
	
	local adders = data.adders
	local addition_total = data.addition_total
		
	local old_modifier, new_modifier = adders[id] or {}, values or {}
	addition_total = addition_total or {}
	
	adders[id] = new_modifier
	
	for property, old_value in pairs(addition_total) do
		data.addition_total[property] = old_value + (new_modifier[property] or 0) - (old_modifier[property] or 0)
	end
		
	player:set_physics_override(recalculate_override(name))
	return true
end

physicsh.get_multiplier = function (name, multiplier)
	if not name then return end
	
	local player
	if type(name) ~= "string" then
		player = name
		name = player:get_player_name()
	else
		player = minetest.get_player_by_name(name)
		if not player then return end
	end
	
	if not internal[name] then return end
	if not multiplier then return end
	
	if not internal[name].multipliers[multiplier] then return end
				
	return table.copy(internal[name].multipliers[multiplier])
end
physicsh.get_adder = function (name, adder)
	if not name then return end
		
	local player
	if type(name) ~= "string" then
		player = name
		name = player:get_player_name()
	else
		player = minetest.get_player_by_name(name)
		if not player then return end
	end
	
	if not internal[name] then return end
	if not adder then return end
	
	if not internal[name].adders[adder] then return end
				
	return table.copy(internal[name].adders[adder])
end

physicsh.recalculate = function (name)
	if not name then return end
		
	local player
	if type(name) ~= "string" then
		player = name
		name = player:get_player_name()
	else
		player = minetest.get_player_by_name(name)
		if not player then return end
	end
	
	local o = recalculate_override(name, true)
	player:set_physics_override(o)
	return o
end

minetest.register_craftitem("physicsh:axe", {
	description = "Test Axe (left to apply effect, right to remove it)",
	
	groups = {not_in_creative_inventory = 1},
	
	inventory_image = "default_tool_meseaxe.png",
	liquids_pointable = true,

	on_use = function(itemstack, user, pointed_thing)
		local name = user:get_player_name()
		
		physicsh.set_multiplier(user, "physicsh:axe_speed", {speed = 2})
		physicsh.set_adder(name, "physicsh:axe_jump", {jump = 0.5})
	end,

	on_place = function(itemstack, user, pointed_thing)
		local name = user:get_player_name()

		physicsh.set_multiplier(name, "physicsh:axe_speed")
		physicsh.set_adder(user, "physicsh:axe_jump")
	end,
})
